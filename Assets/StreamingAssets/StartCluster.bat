::Set n to number of clients
set /A n=2

::Start server
start CR.exe -server %n% tcp://*:1234 tcp://*:* 200000 -logFile server.txt -popupwindow
timeout 3

::Start clients
set /A n=%n%-1
for /l %%x in (0, 1, %n%) do (
start CR.exe -client %%x tcp://127.0.0.1:1234 tcp://*:* 200000 -logFile client%%x.txt -popupwindow
timeout 3
)