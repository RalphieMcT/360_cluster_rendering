﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProgressPanel : MonoBehaviour
{

    Image progressBar;
    public VideoImporter videoImporter;
    Animator progressPanelAnim;

    void Start()
    {
        progressPanelAnim = GetComponent<Animator>();
        progressBar = GameObject.Find("Canvas/ProgressPanel/ProgressBar").GetComponent<Image>();
    }

    void FixedUpdate()
    {
        if (videoImporter != null)
        {
            if (videoImporter.videoImportInfo.isImporting)
                SetProgressBarValue(videoImporter.videoImportInfo.Progress);
            else if (!videoImporter.videoImportInfo.isImporting)
                HideProgressBar();
        }
    }

    public void StartProgressBar(VideoImporter videoImporter)
    {
        this.videoImporter = videoImporter;
    }

    public void HideProgressBar()
    {
        progressPanelAnim.SetBool("ProgressBarVisible", false);
    }

    void SetProgressBarValue(double progressValue)
    {
        progressPanelAnim.SetBool("ProgressBarVisible", true);
        progressBar.fillAmount = (float)progressValue;
    }
}
