﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityStandardAssets.ImageEffects;

[ExecuteInEditMode]
public class Stencil : ImageEffectBase
{
    
    protected override void Start()
    {
        base.Start();
        string fullTexturePath = RelativeToFullPath(gameObject.name);
        Texture2D stencilTexture = TextureLoader.LoadRawTexture(fullTexturePath, TextureFormat.ARGB32);
        material.SetTexture("_StencilTex", stencilTexture);
    }

    private string RelativeToFullPath(string cameraName)
    {
        string startOfPath = Path.Combine(Application.dataPath, @"StreamingAssets/CameraFiles");
        startOfPath = Path.Combine(startOfPath, cameraName);
        string endOfPath = gameObject.GetComponent<CameraConfig>().StencilTexture;
        endOfPath = endOfPath.Substring(2, endOfPath.Length - 2);
        string fullTexturePath = Path.Combine(startOfPath, endOfPath);
        return fullTexturePath;
    }

    void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        Graphics.Blit(source, destination, material);
    }
}
