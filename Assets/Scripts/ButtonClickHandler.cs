﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonClickHandler : MonoBehaviour {

	public void OnClick()
    {
        VideoPlayer videoPlayer;
        GameObject videoWall = GameObject.Find("VideoWall");
        if (transform.name.EndsWith(".ogg"))
        {
            if(videoWall.gameObject.GetComponent<OggViewer>() == null)
            {
                videoPlayer = videoWall.AddComponent<OggViewer>();
            }
            else
            {
                videoPlayer = videoWall.GetComponent<OggViewer>();
            }
            videoPlayer.Initialize(transform.name);
        }
        else
        {
            if(videoWall.gameObject.GetComponent<DDSPlayer>() == null)
            {
                videoPlayer = videoWall.AddComponent<DDSPlayer>();
            }
            else
            {
                videoPlayer = videoWall.GetComponent<DDSPlayer>();
            }
            videoPlayer.Initialize(transform.name);
        }
        //TODO: investigate and fix using ogg then jpeg videoplayer
        //GameObject.Find("Expand").GetComponent<PanelExpander>().ToggleVisible();
    }
}
