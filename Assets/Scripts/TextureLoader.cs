﻿/* 
 * This class loads a DDS file into a Texture2D and returns it
 * http://answers.unity3d.com/questions/555984/can-you-load-dds-textures-during-runtime.html
 * Modified by Rolf-Magnus Hjørungdal to handle R32G32B32A32_FLOAT
 */
 
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public static class TextureLoader {

    public static Texture2D LoadRawTexture(string texturePath, TextureFormat textureFormat)
    {
        Texture2D texture;
        byte[] bytes = File.ReadAllBytes(texturePath);
        texture = LoadTexture(bytes, textureFormat);
        return texture;
    }

    private static Texture2D LoadTexture(byte[] ddsBytes, TextureFormat textureFormat)
    {
        byte ddsSizeCheck = ddsBytes[4];
        if (ddsSizeCheck != 124)
            throw new Exception("Invalid DDS DXTn texture. Unable to read");  //this header byte should be 124 for DDS image files

        int height = ddsBytes[13] * 256 + ddsBytes[12];
        int width = ddsBytes[17] * 256 + ddsBytes[16];

        int DDS_HEADER_SIZE = 128;
        byte[] dxtBytes = new byte[ddsBytes.Length - DDS_HEADER_SIZE];

        Buffer.BlockCopy(ddsBytes, DDS_HEADER_SIZE, dxtBytes, 0, ddsBytes.Length - DDS_HEADER_SIZE);
        Texture2D texture = new Texture2D(width, height, textureFormat, false);
        texture.LoadRawTextureData(dxtBytes);
        texture.Apply();

        return texture;
    }
}
