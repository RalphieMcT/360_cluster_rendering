﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml;
using System.IO;
using System.Linq;

public class CameraSpawner : MonoBehaviour
{
    public GameObject cameraPrefab;
    public enum DisplayType
    {
        SingleMonitor,
        DoubleMonitor,
        MultiProjector,
        ClusterRendering,
        MultiProjectorDebug
    }
    int clientID;
    [SerializeField]
    DisplayType type;
    [SerializeField]
    private int forcedIndex;
    [SerializeField]
    private bool overrideNodeIndex = false;

    void Start()
    {
        clientID = GetClientID();
        CameraRigConfig cameraRigConfig = new CameraRigConfig(type);
        SpawnCameras(cameraRigConfig);
        ActivateDisplays(cameraRigConfig);
    }

    private void ActivateDisplays(CameraRigConfig cameraRigConfig)
    {
        for (int i = 0; i < Display.displays.Length; i++)
        {
            Display.displays[i].Activate();
        }        
    }
    
    private void SpawnCameras(CameraRigConfig cameraRigConfig)
    {

        for (int i = 0; i < cameraRigConfig.cameraConfigPaths.Length; i++)
        {
            if(clientID == i)
            {
                GameObject camera = Instantiate(cameraPrefab);
                camera.transform.parent = transform;
                camera.name = new DirectoryInfo(cameraRigConfig.cameraConfigPaths[i]).Name; //name of current camera directory
                string MainConfig = cameraRigConfig.cameraFilesPath + @"\" + camera.name + @"\mainConfig.xml";
                camera.GetComponent<CameraConfig>().LoadCameraConfiguration(MainConfig);
                camera.transform.localPosition = camera.GetComponent<CameraConfig>().Position;
                camera.transform.eulerAngles = -RadiansToDegrees(camera.GetComponent<CameraConfig>().Rotation);
            }
        }

        //remove default camera
        if (transform.FindChild("Camera").gameObject != null)
            DestroyImmediate(transform.FindChild("Camera").gameObject);

        //turn on warping blending and stencil shaders
        Camera[] cams = FindObjectsOfType<Camera>();
        Debug.Log(cams.Length);
        foreach (Camera c in cams)
        {
            c.gameObject.GetComponent<Warping>().enabled = true;
            c.gameObject.GetComponent<Blending>().enabled = true;
            c.gameObject.GetComponent<Stencil>().enabled = true;
        }
    }

    private Vector3 RadiansToDegrees(Vector3 radVector)
    {
        Vector3 degreeVector = new Vector3();
        degreeVector.x = Mathf.Rad2Deg * radVector.x;
        degreeVector.y = Mathf.Rad2Deg * radVector.y;
        degreeVector.z = Mathf.Rad2Deg * radVector.z;
        return degreeVector;
    }

    private string GetCmdArguments(string arg)
    {
        string[] arguments = Environment.GetCommandLineArgs();
        for (int i = 0; i < arguments.Length; i++)
        {
            if (arguments[i] == arg)
            {
                if (i + 1 < arguments.Length)
                    return arguments[i + 1];
            }
        }
        // default to null
        return null;
    }

    private int GetClientID()
    {
        int clientID;
        string cmdIndex = GetCmdArguments("-client");

        if (cmdIndex != null)
            clientID = int.Parse(cmdIndex);
        else if (overrideNodeIndex)
            clientID = forcedIndex;
        else
            clientID = forcedIndex;

        return clientID;
    }
}
