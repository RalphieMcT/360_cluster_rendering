﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class PanelExpander : MonoBehaviour {
    public Animator optionsPanel;
    public PanelButtons pb;

    public void ToggleVisible() {
        bool currentState = optionsPanel.GetBool("isExpanded");
        if(!currentState) {
            pb.updateFileList();
        }
        optionsPanel.SetBool("isExpanded", !currentState);
    }
}
