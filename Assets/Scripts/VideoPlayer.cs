﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

abstract public class VideoPlayer : MonoBehaviour
{
    protected Renderer sphereRenderer;
    protected AudioSource sphereAudio;
    public bool loopVideo = false;
    public bool IsPlaying { get; set; }
    public string defaultMovieDirectory;

    protected virtual void Awake()
    {
        sphereAudio = GetComponent<AudioSource>();
        sphereRenderer = GetComponent<Renderer>();
        GameObject playButton = GameObject.Find("Canvas/VideoControls/Play");
        GameObject stopButton = GameObject.Find("Canvas/VideoControls/Stop");
        GameObject pauseButton = GameObject.Find("Canvas/VideoControls/Pause");
        playButton.GetComponent<Button>().onClick.AddListener(() => { Play(); });
        stopButton.GetComponent<Button>().onClick.AddListener(() => { Stop(); });
        pauseButton.GetComponent<Button>().onClick.AddListener(() => { Pause(); });
    }

    abstract public void Stop();

    abstract public void Play();
    
    abstract public void Pause();

    abstract public void Initialize(string name);
}
