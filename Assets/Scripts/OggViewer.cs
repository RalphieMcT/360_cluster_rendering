﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.UI;


public class OggViewer : VideoPlayer
{
    protected override void Awake()
    {
        base.Awake();
        defaultMovieDirectory = Path.Combine(Application.streamingAssetsPath, "OggVideos");
    }

    void Update()
    {
        if (typeof(MovieTexture).IsInstanceOfType(sphereRenderer.material.mainTexture))
        {
            IsPlaying = ((MovieTexture)sphereRenderer.material.mainTexture).isPlaying;
        }
    }

    public IEnumerator LoadVideo(string oggFile)
    {
        //is texture a MovieTexture?
        if(typeof(MovieTexture).IsInstanceOfType(sphereRenderer.material.mainTexture))
        {
            ((MovieTexture) sphereRenderer.material.mainTexture).Stop();
        }
        var www = new WWW("file:///" + Path.Combine(defaultMovieDirectory, oggFile));
        yield return www;
        if(www.movie.isReadyToPlay)
            SetVideo(www.movie);
    }
    
    public override void Stop()
    {
        ((MovieTexture) sphereRenderer.material.mainTexture).Stop();
        sphereAudio.Stop();
    }

    public override void Play()
    {
        ((MovieTexture) sphereRenderer.material.mainTexture).Play();

        //play the audio too
        if(sphereAudio.time > 0)
        { 
            sphereAudio.UnPause();
        }
        else
        {
            sphereAudio.Play();
        }
    }

    public override void Pause()
    {
        ((MovieTexture) sphereRenderer.material.mainTexture).Pause();
        sphereAudio.Pause();
    }

    private void SetVideo(MovieTexture movieTexture)
    {
        sphereRenderer.material.mainTexture = movieTexture;
        sphereAudio.clip = movieTexture.audioClip;
        Play();
    }

    public override void Initialize(string name)
    {
        StartCoroutine(LoadVideo(name));
    }
}
