﻿/*
Based on http://bernieroehl.com/360stereoinunity/
Modified by Rolf to work in Unity 5 for non-stereoscopic videos
*/

using System.IO;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Drawing;
using System.Diagnostics;
using System.Linq;

public class DDSPlayer : VideoPlayer
{
    private int secondsToBuffer = 15;
    public float frameRate = 30;
    private int numFramesToBuffer;
    private int numFramesInVideo;
    private Queue<Texture2D> videoTextureBuffer;
    public Stopwatch videoTime;
    int currentFrame;
    string[] frames;
    private string lastInQueue;
    private IEnumerator frameLoaderSubroutine;

    protected override void Awake()
    {
        base.Awake();
        numFramesToBuffer = secondsToBuffer * (int) frameRate;
        videoTime = new Stopwatch();
        videoTextureBuffer = new Queue<Texture2D>();
        currentFrame = 0;
        loopVideo = true;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.S))
            Seek(0.5f);

        if (IsPlaying == false)
            return;

        if ((int)(videoTime.Elapsed.TotalSeconds * frameRate) > currentFrame)
        {
            SetNewFrame();
        }

        if (currentFrame >= numFramesInVideo)
        {
            if (loopVideo)
            {
                Stop();
                Play();
            }
            else
            {
                Stop();
            }
        }
    }

    public void Seek(float normalizedTime)
    {
        Stop();
        videoTime.Elapsed.Add(new TimeSpan(0, 0, 0, 0, normalizedTimeToMilliseconds(normalizedTime)));
        currentFrame = ConvertTimestampToFrameNumber(videoTime.Elapsed);
        RestartFrameLoader();

        sphereAudio.time = (float)videoTime.Elapsed.TotalSeconds;
        Play();
    }

    private void RestartFrameLoader()
    {
        StopCoroutine(frameLoaderSubroutine);
        frameLoaderSubroutine = LoadVideo(frames);
        UnloadTextures(videoTextureBuffer);
        StartCoroutine(frameLoaderSubroutine);
    }

    private void UnloadTextures(Queue<Texture2D> videoTextureBuffer)
    {
        foreach (Texture2D item in videoTextureBuffer)
        {
            Destroy(item);
        }
        videoTextureBuffer.Clear();
    }

    private int normalizedTimeToMilliseconds(float normalizedTime)
    {
        return (int)((normalizedTime * numFramesInVideo) * (100 / frameRate));
    }

    private void SetNewFrame()
    {
        currentFrame = ConvertTimestampToFrameNumber(videoTime.Elapsed);
        Texture2D previousTexture = (Texture2D)sphereRenderer.material.mainTexture;
        sphereRenderer.material.mainTexture = videoTextureBuffer.Dequeue();

        if (previousTexture != null)
            UnityEngine.Object.Destroy(previousTexture);
    }

    private int ConvertTimestampToFrameNumber(TimeSpan time)
    {
        return (int)(time.TotalSeconds * frameRate);
    }

    public override void Play()
    {
        if (!IsPlaying)
        {
            videoTime.Start();
            sphereAudio.Play();
            IsPlaying = true;
        }
    }

    public override void Pause()
    {
        videoTime.Stop();
        sphereAudio.Pause();
        IsPlaying = false;
    }

    public override void Stop()
    {
        videoTime.Reset();
        sphereAudio.Stop();
        IsPlaying = false;
        currentFrame = 0;
    }

    IEnumerator LoadAudio(string DDSFolderPath)
    {
        UnityEngine.Debug.Log(DDSFolderPath + "/audio.wav");
        WWW www = new WWW("file:///" + DDSFolderPath + "/audio.wav");
        yield return www;
        sphereAudio.clip = www.audioClip;
    }

    IEnumerator LoadVideo(string[] frames)
    {
        videoTextureBuffer = new Queue<Texture2D>();
        while (true)
        {
            if (videoTextureBuffer.Count < numFramesToBuffer)
            {
                string nextFramePath = GetNextFrameNotInBuffer(videoTextureBuffer, frames);
                videoTextureBuffer.Enqueue(TextureLoader.LoadRawTexture(nextFramePath, TextureFormat.DXT1));
            }
            yield return new WaitForEndOfFrame();    
        }
    }

    private string GetNextFrameNotInBuffer(Queue<Texture2D> videoTextureBuffer, string[] frames)
    {
        int frameNumberNotInBuffer = (currentFrame + videoTextureBuffer.Count + 1) % numFramesInVideo;
        lastInQueue = frames[frameNumberNotInBuffer];
        lastInQueue = Path.GetFileName(lastInQueue);
        return frames[frameNumberNotInBuffer];
    }

    public override void Initialize(string name)
    {
        defaultMovieDirectory = Path.Combine(Application.streamingAssetsPath, "DDSVideos");
        string DDSFolderPath = Path.Combine(defaultMovieDirectory, name);
        frames = Directory.GetFiles(DDSFolderPath, "*.dds");
        numFramesInVideo = frames.Length;
        StartCoroutine(LoadAudio(DDSFolderPath));
        frameLoaderSubroutine = LoadVideo(frames);
        StartCoroutine(frameLoaderSubroutine);

    }
    
    void OnGUI()
    {
        GUI.Label(new Rect(10, 40, 500, 30), "Displays: " + Display.displays.Length);
        GUI.Label(new Rect(10, 70, 500, 30), "currentFrame: " + currentFrame);
        GUI.Label(new Rect(10, 100, 500, 30), "IsPlaying: " + IsPlaying);
        GUI.Label(new Rect(10, 130, 500, 30), "FramesInBuffer: " + videoTextureBuffer.Count);
        GUI.Label(new Rect(10, 160, 500, 30), "LastInQueue: " + lastInQueue);
        GUI.Label(new Rect(10, 190, 500, 30), "IsRunning: " + videoTime.IsRunning);
        GUI.Label(new Rect(10, 220, 500, 30), "videotime: " + videoTime.Elapsed.TotalSeconds);
        GUI.Label(new Rect(10, 250, 500, 30), "normalized to milliseconds: " + normalizedTimeToMilliseconds(0.5f).ToString());
    }
}