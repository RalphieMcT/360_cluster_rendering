﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

//All color conversions are from http://www.chilliant.com/rgb2hsv.html

Shader "Hidden/Blending"
{
	Properties
	{
		_MainTex("Texture", 2D) = "black" {}
		_BlendTex("BlendTexture", 2D) = "black" {}
	}
	SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"
			float Epsilon = 1e-10;
			
			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
				o.uv = v.uv;
				return o;
			}

			float3 RGBtoHCV(in float3 RGB)
			{
				// Based on work by Sam Hocevar and Emil Persson
				float4 P = (RGB.g < RGB.b) ? float4(RGB.bg, -1.0, 2.0 / 3.0) : float4(RGB.gb, 0.0, -1.0 / 3.0);
				float4 Q = (RGB.r < P.x) ? float4(P.xyw, RGB.r) : float4(RGB.r, P.yzx);
				float C = Q.x - min(Q.w, Q.y);
				float H = abs((Q.w - Q.y) / (6 * C + Epsilon) + Q.z);
				return float3(H, C, Q.x);
			}

			float3 RGBtoHSV(in float3 RGB)
			{
				float3 HCV = RGBtoHCV(RGB);
				float S = HCV.y / (HCV.z + Epsilon);
				return float3(HCV.x, S, HCV.z);
			}
			float3 HUEtoRGB(in float H)
			{
				float R = abs(H * 6 - 3) - 1;
				float G = 2 - abs(H * 6 - 2);
				float B = 2 - abs(H * 6 - 4);
				return saturate(float3(R, G, B));
			}

			float3 HSVtoRGB(in float3 HSV)
			{
				float3 RGB = HUEtoRGB(HSV.x);
				return ((RGB - 1) * HSV.y + 1) * HSV.z;
			}

			sampler2D _MainTex;
			sampler2D _BlendTex;

			fixed4 frag (v2f i) : SV_Target
			{
				//Flip texture if DirectX
				#if UNITY_UV_STARTS_AT_TOP
					i.uv.y = 1 - i.uv.y;
				#endif
				//store backups for uv-values
				float4 tmpX = i.uv.x;
				float4 tmpY = i.uv.y;
				//compensate for rotated texture
				i.uv.x = 1 - i.uv.y;
				i.uv.y = tmpX;
				//sample from blend texture
				float4 blendTexture = tex2D(_BlendTex, i.uv).x;
				//uncompensate 
				i.uv.x = tmpX;
				i.uv.y = 1 - tmpY;
				//sample from source texture
				float3 main = tex2D(_MainTex, i.uv);
				float blend = 1 - blendTexture;

				float3 mainHSV = RGBtoHSV(main);
				mainHSV.z = mainHSV.z - blend;
				float3 mainRGB = HSVtoRGB(mainHSV);
				return fixed4(mainRGB.x, mainRGB.y, mainRGB.z, 0.0);
			}

			ENDCG
		}
	}
}
