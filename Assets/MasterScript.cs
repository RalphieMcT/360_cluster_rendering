﻿using UnityEngine;
using System.Collections;

public class MasterScript : MonoBehaviour
{
	void Update ()
    {
        if (ClusterNetwork.isMasterOfCluster && Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
        if(!ClusterNetwork.isMasterOfCluster && ClusterNetwork.isDisconnected && Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
	}

    void OnGUI()
    {
        GUI.Label(new Rect(10, 10, 500, 30), ClusterNetwork.isMasterOfCluster ? "Master" : "Client");
        GUI.Label(new Rect(10, 30, 500, 30), "ID: " + ClusterNetwork.nodeIndex);
        GUI.Label(new Rect(10, 50, 500, 30), "Is Connected: " + !ClusterNetwork.isDisconnected);
    }
}
