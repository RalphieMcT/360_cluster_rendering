﻿using UnityEngine;
using System;
using System.Collections;
using System.Runtime.InteropServices;

public class PlaceWindow : MonoBehaviour
{
#if UNITY_STANDALONE_WIN || UNITY_EDITOR
    [DllImport("user32.dll", EntryPoint = "SetWindowText")]
    public static extern bool SetWindowText(System.IntPtr hwnd, System.String lpString);
    [DllImport("user32.dll", EntryPoint = "SetWindowPos")]
    private static extern bool SetWindowPos(IntPtr hwnd, int hWndInsertAfter, int x, int Y, int cx, int cy, int wFlags);
    [DllImport("user32.dll", EntryPoint = "FindWindow")]
    public static extern IntPtr FindWindow(System.String className, System.String windowName);

    public static void SetPosition(int x, int y, int resX = 0, int resY = 0)
    {
        SetWindowPos(FindWindow(null, "360_cluster_rendering"), 0, x, y, resX, resY, resX * resY == 0 ? 1 : 0);
    }
#endif
    
    void Awake()
    {
        IntPtr windowPtr = FindWindow(null, "360_cluster_rendering");
        MoveWindow();
        SetWindowText(windowPtr, ClusterNetwork.isMasterOfCluster ? "Master" : "Client" + " " + ClusterNetwork.nodeIndex);
    }

    private static void MoveWindow()
    {
        int x, y, cx, cy;
        if (ClusterNetwork.isMasterOfCluster)
        {
            x = 0; 
            y = 0;
            cx = 0;
            cy = 0;
        }
        else
        {
            x = 1920 * ClusterNetwork.nodeIndex;
            y = 0;
            cx = Screen.width;
            cy = Screen.height;
        }
        SetPosition(x, y, cx, cy);
    }

    void OnGUI()
    {
        string str = "Width:\t\t" + Screen.width +
            "\nHeight:\t\t" + Screen.height +
             "\nResolution:\t\t" + Screen.currentResolution;

        GUI.TextArea(new Rect(new Vector2(300f, 300f), new Vector2(400, 400)), str);
    }
}
