﻿using UnityEditor;
using UnityEditor.Callbacks;
using System.IO;
using UnityEngine;

public class BuildPostProcessor
{
    [PostProcessBuild(1)]
    public static void OnPostprocessBuild(BuildTarget target, string pathToBuiltProject)
    {
        pathToBuiltProject = Path.GetDirectoryName(pathToBuiltProject);
        string builtStreamingAssetsPath = Path.Combine(pathToBuiltProject, "CR_Data/StreamingAssets");
        CopyFile(builtStreamingAssetsPath, "StartCluster.bat", pathToBuiltProject);
        CopyFile(builtStreamingAssetsPath, "user32.dll", pathToBuiltProject + "/CR_Data/Plugins");
    }

    private static void CopyFile(string workingDirectory, string file, string toLocation)
    {
        if (File.Exists(toLocation + "/" + file))
            File.Delete(toLocation + "/" + file);
        File.Copy(workingDirectory + "/" + file, toLocation + "/" + file);
    }
}